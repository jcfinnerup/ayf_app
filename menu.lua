module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Ask Menu")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------


portal.checkOwnerOfLocalQuestions()	


------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
background:setFillColor(230, 230, 230)
localGroup:insert(background)

-- Ask BTN
local askBTN = display.newImage("images/randomCircle2.png")
askBTN.x, askBTN.y = display.contentCenterX, display.contentHeight*.4
localGroup:insert(askBTN)

-- Sponson Banner
local sponsoredBanner = display.newImage("images/sponsored.png")
localGroup:insert(sponsoredBanner)

-- Asks BTN
local asksBTN = display.newImage("images/asks.png")
asksBTN.x, asksBTN.y = display.contentWidth*.1, display.contentHeight*.94
localGroup:insert(asksBTN)

-- Friends BTN
local friendsBTN = display.newImage("images/friends.png")
friendsBTN.x, friendsBTN.y = display.contentWidth*.85, display.contentHeight*.94
localGroup:insert(friendsBTN)

------------------------------------------
--				Buttons					--
------------------------------------------


------------------------------------------
--				Functions				--
------------------------------------------


-- Create Chart
local function createChart()	
	
	local startRotation = -90
	for i=1, 20 do	
	
		-- Slice Data
		local newSlice 
		if i%math.random(3) == 0 then
			newSlice = display.newImage("images/blueSlice.png")
		elseif i%math.random(2) == 0 then
			newSlice = display.newImage("images/greenSlice.png")
		else
			newSlice = display.newImage("images/orangeSlice.png")
		end
		newSlice:setReferencePoint(display.TopLeftReferencePoint) -- Graphics 1
		newSlice.x, newSlice.y = display.contentCenterX, display.contentHeight*.4
		newSlice.rotation = startRotation		
		newSlice:scale(0.0, 0.0)
		newSlice.alpha = 0.8
		transition.to(newSlice, {time=1000, delay=i*30, xScale=1, yScale=0.6 , transition=easing.outExpo})
		transition.to(newSlice, {time=1000, delay=i*5, rotation=startRotation+(360/20)*(i-1), transition=easing.inOutExpo})
		localGroup:insert(newSlice)
				
		-- Hide Overlap
		if i==20 then
			newSlice:toBack()
			background:toBack()
			askBTN:toFront()
		end
	end
end
createChart()

-- Ask  BTN
local function goAskYourFriends(e)
	if e.phase == "began" then
		e.target.alpha = .8
		display.getCurrentStage():setFocus(e.target)
	elseif e.phase == "ended" then
		e.target.alpha = 1
		display.getCurrentStage():setFocus(e.target)
		director:changeScene("writeQuestion", "moveFromRight")
	end
end
askBTN:addEventListener("touch", goAskYourFriends)

-- Go Friends
local function goFriends(e)
	if e.phase == "began" then
		e.target.alpha = .8
		display.getCurrentStage():setFocus(e.target)
	elseif e.phase == "ended" then
		e.target.alpha = 1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("friendList", "fade")
	end
end
friendsBTN:addEventListener("touch", goFriends)

-- Go Ask Wall
local function goAskWall(e)
	if e.phase == "began" then
		e.target.alpha = .8
		display.getCurrentStage():setFocus(e.target)
	elseif e.phase == "ended" then
		e.target.alpha = 1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("askWall", "moveFromLeft")
	end
end
asksBTN:addEventListener("touch", goAskWall)

-- Sponsors
local function goSponsored(e)
	if e.phase == "began" then
		e.target.alpha = .5
	elseif e.phase == "ended" then
		e.target.alpha = 1
	end
end
sponsoredBanner:addEventListener("touch", goSponsored)

-------------- BELOW DIRECTOR -------------
	return localGroup
end