module(..., package.seeall)

local questionParams

-- Set Question Params
function setQuestionParams(params)
	print("Setting Question Params")
	questionParams = params
end

-- New (for director)
function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Results")

------------------------------------------
--			   Requirements				--
------------------------------------------


------------------------------------------
--				Variables				--
------------------------------------------

-- Number Of Slices
local numSlices = 18

-- Question Parameters
local nameOfAsker 	= questionParams["usernameOfSender"]
local questionText 	= questionParams["completeQuestion"]
local questionID 	= questionParams["questionID"]
local myUsername 	= portal.getCurrentUserName()
local bannerUrl 	= "images/resultsBannerGreen.png"
local titleText 	= nameOfAsker.." asks"

-- At Least One - Used to log different Color if nobody has answered
local atLeastOne = false

-- Is Asker You?
if nameOfAsker == myUsername then
	bannerUrl = "images/resultsBannerBlue.png"
	titleText = "You ask"
end

-- Answer Parameters
local answer1 = questionParams["possibleAnswers"][1]
local answer2 = questionParams["possibleAnswers"][2]

-- Score
local score1 = #questionParams["actualAnswers"][1]+0
local score2 = #questionParams["actualAnswers"][2]+0

-- Checking For Zeros
if questionParams["actualAnswers"][1][1] == "0" then
	print("Replacing Zero")
	score1 = 0
end

-- Checking For Zeros
if questionParams["actualAnswers"][2][1] == "0" then
	print("Replacing Zero")
	score2 = 0
end


-- Loggin Percentages
local percentOfFirstScore = math.round((score1/(score1+score2))*100)
local percentOfSecondScore = math.round(100-percentOfFirstScore)

-- Checking Percentages
if score1 == 0 and score2 == 0 then
	percentOfFirstScore	 = 0
	percentOfSecondScore = 0
else
	atLeastOne = true
end

-- Slice Colors
local sliceColor1 = "images/orangeSlice.png" 
local sliceColor2 = "images/blueSlice.png" 
local sliceColor3 = "images/greenSlice.png"

-- Height for Chart
local yChart = display.contentCenterY+50
------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
background:setFillColor(255, 255, 255)
localGroup:insert(background)

-- Results Banner
local resultsBanner = display.newImage(bannerUrl)
localGroup:insert(resultsBanner)

-- Title
local title = display.newText(titleText, 130, resultsBanner.y, "PTSans-Bold", 60)
localGroup:insert(title)

-- Question 
local question = display.newText(questionText, 30, resultsBanner.y*2+30,580,0, "PTSans-Regular", 40)
question:setTextColor(50, 50, 50)
localGroup:insert(question)
question:setReferencePoint(display.BottomLeftReferencePoint)

-- Question Background
local questionBackground = display.newImage(bannerUrl)
questionBackground.y = question.y-questionBackground.height*.5+20
questionBackground.alpha = 0.8
localGroup:insert(questionBackground)

-- Fronting stuff
question:toFront()
resultsBanner:toFront()
title:toFront()

-- Logo Circle 
local logoCircle = display.newImage("images/randomCircle2.png")
logoCircle.x, logoCircle.y = display.contentCenterX, yChart
localGroup:insert(logoCircle)

-- Back Button
local backBTN = display.newImage("images/backButton2.png")
backBTN:translate(15,resultsBanner.y*.63)
localGroup:insert(backBTN)

-- Question Slider 1
local questionSlider1 = display.newImage("images/questionSlider.png")
questionSlider1:setFillColor(255, 170, 71)
questionSlider1.alpha = 0.8
questionSlider1.x, questionSlider1.y = display.contentCenterX, display.contentHeight-160

-- Question Slider 2
local questionSlider2 = display.newImage("images/questionSlider.png")
questionSlider2:setFillColor(85, 139, 199)
questionSlider2.x, questionSlider2.y = display.contentCenterX, display.contentHeight-70

-- First Answer
local firstAnswer = display.newText(answer1, 60, 0, "PTSans-Bold", 30)
firstAnswer.y = questionSlider1.y
firstAnswer:setTextColor(50, 50, 50)

-- Second Answer
local secondAnswer = display.newText(answer2, 60, 0, "PTSans-Bold", 30)
secondAnswer.y = questionSlider2.y
secondAnswer:setTextColor(50, 50, 50)

-- Percent Of First Answer Text
local percentOfFirstText = display.newText(percentOfFirstScore.."%", 0, 0, "PTSans-Bold", 30)
percentOfFirstText:setTextColor(50, 50, 50)
percentOfFirstText.x, percentOfFirstText.y = display.contentWidth*0.85, questionSlider1.y

-- Percent Of Second Answer Text
local percentOfSecondText = display.newText(percentOfSecondScore.."%", 0, 0, "PTSans-Bold", 30)
percentOfSecondText:setTextColor(50, 50, 50)
percentOfSecondText.x, percentOfSecondText.y = display.contentWidth*0.85, questionSlider2.y

-- Answer Groups
local answerGroup1 = display.newGroup()
answerGroup1.type = 1
local answerGroup2 = display.newGroup()
answerGroup2.type = 2

answerGroup1:insert(questionSlider1)
answerGroup2:insert(questionSlider2)
answerGroup1:insert(firstAnswer)
answerGroup2:insert(secondAnswer)
answerGroup1:insert(percentOfFirstText)
answerGroup2:insert(percentOfSecondText)

-- Chart Group
local chartGroup = display.newGroup()
------------------------------------------
--				Buttons					--
------------------------------------------


------------------------------------------
--				Functions				--
------------------------------------------

-- Reset Chart Group
local function resetChartGroup()
	chartGroup:removeSelf()
	chartGroup = nil
	chartGroup = display.newGroup()
end

-- Create Chart
local function createChart(firstColorLimit)	
	
	local startRotation = -90
	for i=1, numSlices do	
		
		-- Select Slice Color
		local sliceColor = sliceColor1
		if i>firstColorLimit and atLeastOne == true then
			sliceColor = sliceColor2
		elseif atLeastOne == false and sliceColor == sliceColor1 then
			sliceColor = sliceColor3
		end
	
		-- Slice Data
		local newSlice = display.newImage(sliceColor)
		newSlice:setReferencePoint(display.TopLeftReferencePoint) -- Graphics 1
		newSlice.x, newSlice.y = display.contentCenterX, yChart
		newSlice.rotation = startRotation		
		newSlice:scale(0.0, 0.0)
		newSlice.alpha = 0.8
		transition.to(newSlice, {time=1000, delay=i*30, xScale=0.8, yScale=0.6 , transition=easing.outExpo})
		transition.to(newSlice, {time=1000, delay=i*5, rotation=startRotation+(360/numSlices)*(i-1), transition=easing.inOutExpo})
		chartGroup:insert(newSlice)
		
		-- Hide Overlap
		if i==numSlices then
			localGroup:insert(chartGroup)
			newSlice:toBack()
			background:toBack()
			logoCircle:toFront()						
		end
	end
end

-- Display Chart - Calculate Chart Data
local function displayChart()
	local percentOfFirstAnswer = numSlices
	if score1 == 0 and score2 == 0 then
		print("Results: nan")
	elseif score1 == 1 and score2 == 0 then
		print("Results: inf large")
		percentOfFirstAnswer = 24
	elseif score1 == 0 and score2 == 1 then
		print("Results: inf small")
		percentOfFirstAnswer = 0
	else
		print("Results: Fair")
		percentOfFirstAnswer = math.round((score1/(score1+score2))*numSlices)
	end
	resetChartGroup()
	createChart(percentOfFirstAnswer)
	
end
displayChart()

-- Back To Ask Wall
local function backToAskWall(e)
	if e.phase == "began" then
		display.getCurrentStage():setFocus(backBTN)
		backBTN.alpha=0.8
	elseif e.phase == "ended" then
		backBTN.alpha=1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("askWall")
	end
end
backBTN:addEventListener("touch", backToAskWall)

-- Update Question Params
local function updateQuestionParams(answerIndex)
	atLeastOne = true
	if answerIndex == 1 then
		score1 = score1 + 1
	elseif answerIndex == 2 then
		score2 = score2 + 1	
	end
	
	-- Updating %
	percentOfFirstScore = math.round((score1/(score1+score2))*100)
	percentOfSecondScore = math.round(100-percentOfFirstScore)
	
	-- Updating Text
	percentOfFirstText.text = percentOfFirstScore.."%"
	percentOfSecondText.text = percentOfSecondScore.."%"
end

-- Submit Answer
local function submitAnswer(target, functionToRemove)
	local hasResponded = portal.submitAnswerToQuestion(questionID, target.type)
	if hasResponded == "false" then
		print("Has Already Answered Question")
	elseif hasResponded == "noconnect" then
		print("No Connection")
	else
		print("Success!")
		updateQuestionParams(target.type)
		displayChart()	
		answerGroup1:removeEventListener("touch", functionToRemove)
		answerGroup2:removeEventListener("touch", functionToRemove)		
	end
end

-- Answer On Touch
local xBuffer = 0
local function answerOnTouch(e)
	if e.phase == "began" then
		xBuffer = 0-e.x
		display.getCurrentStage():setFocus(e.target)
		e.target.alpha=0.8
	elseif e.phase == "moved" and xBuffer ~= 0 then
		e.target.x = e.x+xBuffer
	elseif e.phase == "ended" then
		transition.to(e.target,{x=0, transition=easing.outExpo})
		e.target.alpha=1
		display.getCurrentStage():setFocus(nil)
		xBuffer = 0
		submitAnswer(e.target, answerOnTouch)
	end
end
answerGroup1:addEventListener("touch", answerOnTouch)
answerGroup2:addEventListener("touch", answerOnTouch)




-------------- BELOW DIRECTOR -------------
	return localGroup
end