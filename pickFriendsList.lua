module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Pick Friends")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------

-- My Name For Reference
local myName = portal.getCurrentUserName()

-- My Email For Reference
local myEmail = portal.getCurrentUserEmail()

-- Number Of Friends
local numberOfFriends = 0

-- Receiver Table
local receiverTable = {}
------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
background:setFillColor(230, 230, 230)
localGroup:insert(background)

-- Friend Banner
local friendBanner = display.newImage("images/friendBanner.png")
friendBanner.x = display.contentCenterX
localGroup:insert(friendBanner)

-- Back BTN
local backToAskBTN = display.newImage("images/backToAskOrange.png")
backToAskBTN:translate(15,friendBanner.y*.63)
localGroup:insert(backToAskBTN)

-- Ask Banner
local askBanner = display.newImage("images/askBanner.png")
askBanner.x, askBanner.y = display.contentCenterX, display.contentHeight-50
askBanner.isVisible = false
localGroup:insert(askBanner)

-- Add Circle BTN
local addCircleBTN = display.newImage("images/addCircle.png")
addCircleBTN.x, addCircleBTN.y = 570, 50
addCircleBTN.alpha = 0
localGroup:insert(addCircleBTN)

-- No Friends Text
local noFriendsText = display.newImage("images/noFriendsText.png")
noFriendsText.x, noFriendsText.y = display.contentCenterX, display.contentCenterY
noFriendsText.isVisible = false
localGroup:insert(noFriendsText)

----------------------------------------------
--		Table View Configuration w. func	--
----------------------------------------------

-- The TableView Variable For Reference
local questionTableView

-- Function For Reference
local downloadFriendList

-- Display Or Hide Ask Banner
local yBannerConstant = askBanner.y
local function displayOrHideAskBanner()	
	local numberPicked = table.maxn(receiverTable)
	if numberPicked == 0 then
		askBanner.isVisible = false
	elseif askBanner.isVisible == false then
		askBanner.isVisible = true
	end
end

-- Pick Friend
local function pickUnpickFriend(row)
	local picked = row.picked
	if picked.isVisible == true then
		picked.isVisible = false 
		receiverTable[row.index] = nil
	else
		receiverTable[row.index] = row
		picked.isVisible = true 
	end
	displayOrHideAskBanner()
end

-- Accept Friend Request
local function acceptFriendRequest(friendEmail)
	print("Accepting Friend Request")
	local hasBeenAccepted = portal.accpectFriendRequest(friendEmail)
	if hasBeenAccepted == "true" then
		downloadFriendList()
		print("Updating Friend List")
	else
		promt.promtUserWithMessage("Oooops", "You cannot answer your friend request now. Please check your internet connection")
	end
end

-- Table View Touch
local function tableViewTouch(e)
	if (e.phase == "tap" or e.phase ==  "release") then
		pickUnpickFriend(e.target)
	end
end

-- ScrollListener
local function scrollListener(e)
	-- Nothing
end

-- Row Rendering Options
-- this is what happenes with each individual row
local function renderTableViewTaps(e)
	
	-- Reference data
	local row = e.row
	local friendData = row.params
	local requesterUsername = 	friendData["requesterUsername"]
	local requesterEmail = 		string.lower(friendData["requesterEmail"])
	local receiverUsername = 	friendData["receiverUsername"]
	local receiverEmail = 		string.lower(friendData["receiverEmail"])
	local status = 				friendData["status"]
	local nameToDisplay = "Unknown"
	
	-- Picking Name to display and setting Email
	if myEmail == requesterEmail then
		nameToDisplay = receiverUsername
		row.email = receiverEmail
	else
		nameToDisplay = requesterUsername
		row.email = requesterEmail
	end

	-- Creating Artificial lines between rows
	row.yScale = 0.99
	
	-- Counting number of friends
	numberOfFriends = numberOfFriends + 1
	
	-- Friend Name Text
	local friendNameText = display.newText(row, nameToDisplay, display.contentWidth*0.05, 0, "PTSans-Regular", 35)
	friendNameText:setFillColor(75, 75, 75)
	friendNameText.y =  row.height*.5
	
	-- Pick Friend Button
	local pickFriendButton = display.newImage("images/pickFriendButton.png")
	pickFriendButton:translate(display.contentWidth*.85, row.height*.5-pickFriendButton.height*.5)
	row:insert(pickFriendButton)
	
	-- Picked Friend Button
	local pickedFriendButton = display.newImage("images/pickedFriendButton.png")
	pickedFriendButton.x, pickedFriendButton.y = pickFriendButton.x, pickFriendButton.y
	pickedFriendButton.isVisible = false
	row:insert(pickedFriendButton)
	row.picked = pickedFriendButton
	
end


-- Table View for Friends
-- contains all the settings for the TableView
friendTableView = widget.newTableView
{
	top=friendBanner.y*2,
	left=0,
	width=display.contentWidth,
	height = display.contentHeight-friendBanner.y*2,
	topPadding = 0,
	bottomPadding = display.contentHeight*.5,
	onRowRender = renderTableViewTaps,
	listener = scrollListener,
	onRowTouch = tableViewTouch,
	hideBackground = true,
}
localGroup:insert(friendTableView)
local currentPos = friendTableView:getContentPosition()
friendTableView:scrollToY({y=currentPos, time=0})

-- Fronting Things
askBanner:toFront()

------------------------------------------
--				Functions				--
------------------------------------------

-- Put Friends On Screen
local function putFriendsOnScreen(friends)
	for i = 1, #friends do
	    friendTableView:insertRow{
			params = friends[i],
			rowHeight = 120,
			rowColor={ 
				default={ 255, 255, 255 }, 
				over={ 255, 255, 255, 1 } 
			},
		}
	end
	-- Update row count
	totalRowCount = friendTableView:getNumRows()
	
	-- Set Rows Right
	friendTableView:scrollToY({y=currentPos, time=0})
end

-- Show No Friends
local function showNoFriends()
	noFriendsText.isVisible = true
end

-- Filter Out Pending From Pickable Friends
local function filterPickableFriends(rawTable)
	local newTable = {}
	for i=1, #rawTable do
		if rawTable[i]["status"] == "accepted" then
			table.insert(newTable, rawTable[i])
		end
	end
	return newTable
end

-- Download Friend List
downloadFriendList = function()
	
	-- Reset Number of friends
	numberOfFriends = 0
	
	-- Download questions and react to status
	local friendList = portal.getAllFriends()
	if friendList == "noconnect" then
		promt.promtUserWithMessage("Oooops", "We couldn't download your friendlist. Please check your internet connection and try again")
	elseif friendList == "empty" then
		showNoFriends()
	else
		-- Hide No Friends Sign
		noFriendsText.isVisible = false
		
		-- Delete all Excisting Rows & Adding New
		friendTableView:deleteAllRows()
		local filteredFriends = filterPickableFriends(friendList)
		putFriendsOnScreen(filteredFriends)
	end
end

-- Go Back
local function goBack(e)
	if e.phase == "began" then
		backToAskBTN.alpha=0.8
		display.getCurrentStage():setFocus(backToAskBTN)
	elseif e.phase == "ended" then
		backToAskBTN.alpha=1
		display.getCurrentStage():setFocus(nil)
		director:changeScene(lastScene, "fade")
	end
end
backToAskBTN:addEventListener("touch", goBack)


-- Get People To Send To 
local function getPeopleToSendTo()
	local tempTable = {}
	
	-- Put selected friends in table with email
	for i=1, numberOfFriends do
		if receiverTable[i] ~= nil then
			table.insert(tempTable, receiverTable[i].email)
			print("Sending To "..receiverTable[i].email)
		end		
	end
	return tempTable
end

-- Post The Question via. portal
local function postViaPortal()
	local peopleToSendTo = getPeopleToSendTo()
	local toReturn = 0
	
	-- Tell Portal to send question
	if #peopleToSendTo > 0 then
		toReturn = portal.createAndSendQuestion(peopleToSendTo)
	end
	return toReturn
end

-- Ask Banner on Touch
local function askBannerOnTouch(e)
	local hasQuestionBeenPosted = postViaPortal()
	if hasQuestionBeenPosted ~= 0 then
		director:changeScene("askWall", "fade")
	end
end
askBanner:addEventListener("tap", askBannerOnTouch)


-- Download The Friend List when loaded
downloadFriendList()


-------------- BELOW DIRECTOR -------------
	return localGroup
end