


module(...,package.seeall)
print("JSON: Starting..")

--==========================================--
--==========================================--
--               JSON HANDLER               --
--==========================================--
--==========================================--

-- JSON FILE VARIABLES (To call)
local dataPath		= system.pathForFile( "loginInformation.json", system.DocumentsDirectory )
local questionPath 	= system.pathForFile( "localQuestions.json", system.DocumentsDirectory )
local friendPath 	= system.pathForFile( "localFriends.json", system.DocumentsDirectory )


local file1 = io.open( dataPath, "r" )
local file2 = io.open( questionPath, "r" )
local file3 = io.open( friendPath, "r" )

if not file1  then
	
    print("JSON: file(s) are missing! Creating new files..")
	
	-- Paths
    local deployPath1 = system.pathForFile("JSON/loginInformation.json", system.ResourceDirectory)
    local deployPath2 = system.pathForFile("JSON/localQuestions.json", system.ResourceDirectory)
    local deployPath3 = system.pathForFile("JSON/localFriends.json", system.ResourceDirectory)
	
	-- Opening files
    local file1 = io.open( deployPath1, "r" )
    local file2 = io.open( deployPath2, "r" )
    local file3 = io.open( deployPath3, "r" )
	
	-- Save data
    local saveData1 = file1:read( "*a" )
    local saveData2 = file2:read( "*a" )
    local saveData3 = file3:read( "*a" )
	
	-- Re-closing files
    io.close( file1 )
    io.close( file2 )
    io.close( file3 )
	
    --- Write JSON content into SANDBOX file
    file1 = io.open( dataPath, "w")
    file2 = io.open( questionPath, "w")
    file3 = io.open( friendPath, "w")
	
	-- Write JSON structures into files
    file1:write( saveData1 )
    file2:write( saveData2 )
    file3:write( saveData3 )
	
	-- Closing files
    io.close( file1 )
    io.close( file2 )
    io.close( file3 )
	
	-- Nilling
    file1 = nil
    file2 = nil
    file3 = nil
    
	-- Initialization
    loginInformation   	= json.decode( saveData1 )
    localQuestions   	= json.decode( saveData2 )
	localFriends 		= json.decode( saveData3 )
	
else
    print("JSON: files found!")
	
	-- read all contents of file into a string
	local saveData1 = file1:read( "*a" )
	local saveData2 = file2:read( "*a" )
	local saveData3 = file3:read( "*a" )
    
    -- Saving data in local variables
    loginInformation	=  json.decode( saveData1 )
    localQuestions   	=  json.decode( saveData2 )
	localFriends 		= json.decode( saveData3 )

    -- close the file after using it
	io.close( file1 )
	io.close( file2 )
	io.close( file3 )
end

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--
--         		READ FUNCTIONS        		--
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--

----------------------------------------------
--				Login Information			--
----------------------------------------------
-- Get Current User Information
function getCurrentUserInformation()
    currentUserInfo = loginInformation
	return currentUserInfo;
end

----------------------------------------------
--				Local Questions				--
----------------------------------------------

-- Get All Local Questions
function getAllLocalQuestions()
	local allQuestionData = localQuestions["allQuestions"]
	return allQuestionData
end

-- Get Newest Local Question
function getNewestLocalQuestion()
	local newestQuestion = localQuestions["allQuestions"][1]["questionID"]
	return newestQuestion
end

-- Get Owner Of Local Questions
function getOwnerOfLocalQuestions()
	local owner = localQuestions["owner"]
	return owner
end

----------------------------------------------
--				Local Friends				--
----------------------------------------------

-- Get All Local Friends
function getAllLocalFriends()
	local allFriendData = localFriends["allFriends"]
	return allFriendData
end

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--
--         		WRITE FUNCTIONS        		--
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--

----------------------------------------------
--				Login Information			--
----------------------------------------------

-- Set Current User Information
function setCurrentUserInformation(userID, username, userEmail, userPassword, userPhone)
    loginInformation["currentUserID"] 	= userID;
    loginInformation["currentUsername"] = username;
    loginInformation["currentEmail"] 	= userEmail;
    loginInformation["currentPassword"] = userPassword;
    loginInformation["currentPhone"] 	= userPhone;
	
	local file = io.open( dataPath, "w")
	file:write( json.encode(loginInformation) )
	io.close( file )
	file = nil
end

-- Reset Current User Information
--* for logout
function resetCurrentUserInformation()
	local deployPath1 = system.pathForFile("JSON/loginInformation.json", system.ResourceDirectory)
    local file1 = io.open( deployPath1, "r" )
    local saveData1 = file1:read( "*a" )
    io.close( file1 )
    file1 = io.open( dataPath, "w")
    file1:write( saveData1 )
    io.close( file1 )
    file1 = nil
    loginInformation =  json.decode( saveData1 )
end

----------------------------------------------
--				Local Questions				--
----------------------------------------------

-- Save All Questions
function saveAllQuestions(allQuestionsData, owner)
	localQuestions["allQuestions"] = allQuestionsData
	localQuestions["owner"] = owner  
	local file = io.open( questionPath, "w")
	file:write( json.encode(localQuestions) )
	io.close( file )
	file = nil
end
	
-- Reset Local Question Data
function resetLocalQuestionData()
	local deployPath2 = system.pathForFile("JSON/localQuestions.json", system.ResourceDirectory)
    local file2 = io.open( deployPath2, "r" )
    local saveData2 = file2:read( "*a" )
    io.close( file2 )
    file2 = io.open( questionPath, "w")
    file2:write( saveData2 )
    io.close( file2 )
    file2 = nil
    localQuestions =  json.decode( saveData2 )	
end
	
----------------------------------------------
--				Local Friends				--
----------------------------------------------

-- Save All Friends
function saveAllFriends(allFriendData, owner)
	localFriends["allFriends"] = allFriendData
	localFriends["owner"] = owner  
	local file = io.open( friendPath, "w")
	file:write( json.encode(localFriends) )
	io.close( file )
	file = nil
end

-- Reset Local Friend Data
function resetLocalFriendData()
	local deployPath3 = system.pathForFile("JSON/localFriends.json", system.ResourceDirectory)
    local file2 = io.open( deployPath3, "r" )
    local saveData2 = file2:read( "*a" )
    io.close( file2 )
    file2 = io.open( friendPath, "w")
    file2:write( saveData2 )
    io.close( file2 )
    file2 = nil
    localFriends =  json.decode( saveData2 )	
end
