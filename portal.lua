--[[
Portal Class
This will handle all the bridging from other classes to the dataParser
and the connector for simplicity.
]]

module(..., package.seeall)

print("Portal Ready...")



--------------------------------------------------
--					Requirements				--
--------------------------------------------------

-- Data Parser
local dataParser = require "dataParser"

-- Connector
local connector = require "connector"

-- Json Handler
local jsonHandler = require "jsonHandler"

--------------------------------------------------
--					User Data					--
--------------------------------------------------

-- Current User ID
local currentUserID = 1

-- Current User Password
local currentUserPassword = "morten"

-- Current User Emaik
local currentUserEmail = "liza@.com"

-- Current User Name
local currentUserName = "liza"

-- Current User Phone
local currentUserPhone = "nophone"
--------------------------------------------------
--					Variables					--
--------------------------------------------------

-- Temp Variables for questions
local currentQuestion = "Test question?"
local currentAnswers = {"Test1", "Test2"}
local currentReceivers = {"Ida", "Rene"}

-- Device Token
local deviceToken = "NoTokenRegistered" -- Changes Automatically on device


--------------------------------------------------
--				Setter Functions				--
--------------------------------------------------

-- Set Current Question
function setCurrentQuestion(q)
	currentQuestion = nil
	currentQuestion = q;
end

-- Set Current Answers
function setCurrentAnswers(a)
	currentAnswers = nil
	currentAnswers = a;
end

-- Set Current Receivers
function setCurrentReceivers(r)
	currentReceivers = nil
	currentReceivers = r;
end

-- Set Device Token
function setDeviceToken(token)
	deviceToken = token;
end

--------------------------------------------------
--				Server Functions				--
--------------------------------------------------

-- Create And Send Question
function createAndSendQuestion(friendsToReceive)
	local dataToSend = dataParser.createQuestionData(currentUserID, currentUserName, currentQuestion, currentAnswers, friendsToReceive)
	print("Request: "..dataToSend)	
	local questionNumber = connector.pushToServer(dataToSend)	
	print("Question Number: "..questionNumber)	
end

-- Create New User
function createNewUser(username, password, email, phoneNumber)
	local dataToSend = dataParser.createNewUserData(username, password, email, phoneNumber, deviceToken);
	print("Request: "..dataToSend)
	local hasUserBeenCreated = connector.pushToServer(dataToSend)
	print("New User Created: "..hasUserBeenCreated)
	return hasUserBeenCreated
end

-- Get Questions
function getQuestions()
	local dataToSend = dataParser.createGetAllQuestionsData(currentUserID, currentUserEmail)
	print("Request: "..dataToSend)
	local questionData = connector.pushToServer(dataToSend)
	print("Question Data: "..questionData)
	if questionData ~= "noconnect" then
		questionData = dataParser.parseQuestionData(questionData)
		jsonHandler.saveAllQuestions(questionData, currentUserEmail)
	end
	return questionData
end

-- Verify User
function verifyUser(email, password)
	local dataToSend = dataParser.createVerifyUserData(email, password)
	local verification = connector.pushToServer(dataToSend)
	if verification ~= "false" and verification ~= "noconnect" then
		verification = dataParser.parseDataOnLogin(verification)
	end
	return verification
end

-- Get Complete Friend List
function getAllFriends()
	local dataToSend = dataParser.createGetAllFriendsData(currentUserEmail, currentUserPassword)
	local friendList = connector.pushToServer(dataToSend)
	if friendList ~= "noconnect" and friendList ~= "empty" then
 		friendList = dataParser.parseCompleteFriendList(friendList)
		jsonHandler.saveAllFriends(friendList, currentUserEmail)
	end
	return friendList
end

-- Search For Friends
function searchForFriends(friendData)
	local dataToSend = dataParser.searchForFriends(currentUserName, currentUserEmail, currentUserPassword, friendData)
	local potentialFriends = connector.pushToServer(dataToSend)
	if potentialFriends ~= "noconnect" and potentialFriends ~= "empty" then
 		potentialFriends = dataParser.parseCompleteFriendList(potentialFriends)
	end
	return potentialFriends
end

-- Add Friend
function addFriend(friendUsername, friendEmail)
	local dataToSend = dataParser.createAddFriendData(currentUserName, currentUserEmail, currentUserPassword, friendUsername, friendEmail)
	local hasFriendBeenAdded = connector.pushToServer(dataToSend)
	return hasFriendBeenAdded
end

-- Accept Friend Request
function accpectFriendRequest(friendEmail)
	local dataToSend = dataParser.createAcceptFriendRequestData(currentUserName, currentUserEmail, currentUserPassword, friendEmail)
	local response = connector.pushToServer(dataToSend)
	return response
end

-- Submit Answer To Question
function submitAnswerToQuestion(questionID, answer)
	local dataToSend = dataParser.createDataToSubmitAnswer(currentUserName, currentUserEmail, currentUserPassword, questionID, answer)
	local response = connector.pushToServer(dataToSend)
	return response
end
--------------------------------------------------
--				Getter Functions				--
--------------------------------------------------

-- Get Current User ID
function getCurrentUserID()
	return currentUserID;
end

-- Get Current User Password
function setCurrentUserPassword()
	return currentUserPassword;
end

-- Get Current User Email
function getCurrentUserEmail()
	return string.lower(currentUserEmail);
end

-- Get Current User Name
function getCurrentUserName()
	return currentUserName;
end

-- Get Current Question
function getCurrentQuestion()
	return currentQuestion;
end

-- Get Current Answers
function getCurrentAnswers()
	return currentAnswers;
end

-- Get Current Receivers
function getCurrentReceivers()
	return currentReceivers;
end

-- get Device Token
function getDeviceToken()
	return deviceToken;
end

-- get Name Of Sender()
function getNameOfSender(sender)
	local name = "Who?"
	if string.lower(sender) == string.lower(currentUserName) then
		name = "You"
	else
		name = sender
	end
	return name
end

--------------------------------------------------
--				Setter Functions				--
--------------------------------------------------

-- Set Current User ID
function setCurrentUserID(newUserID)
	print("Current User ID Set!")
	currentUserID = newUserID;
end

-- Set Current User Password
function setCurrentUserPassword(newUserPassword)
	print("Current User Password Set!")
	currentUserPassword = newUserPassword;
end

-- Set Current User Email
function setCurrentUserEmail(newUserEmail)
	print("Current User Email Set!")
	currentUserEmail = newUserEmail;
end

-- Set Current User Name
function setCurrentUserName(newUserName)
	print("Current User Name Set!")
	currentUserName = newUserName;
end


--------------------------------------------------
--				Local JSON Functions			--
--------------------------------------------------

-- Get Local Questions
function getLocalQuestions()
	local localQuestions = jsonHandler.getAllLocalQuestions()
	return localQuestions
end

-- Check Owner Of Local Questions
function checkOwnerOfLocalQuestions()
	if jsonHandler.getOwnerOfLocalQuestions() ~= currentUserEmail then
	 	jsonHandler.resetLocalQuestionData()
		print("Resetting local questions..")
	else
		print("Keeping local questions")
	end
end

-- Get Local Friends
function getLocalFriends()
	local friendData = jsonHandler.getAllLocalFriends()
	return friendData
end

-- Set Current User For Automatic Login
function setCurrentUserForAutomaticLogin()
	print("Setting up automatic login..")
	jsonHandler.setCurrentUserInformation(
		currentUserID, 
		currentUserName, 
		currentUserEmail, 
		currentUserPassword, 
		currentUserPhone
	)
end

-- Get Current User For Automatic Login
function getCurrentUserForAutomaticLogin()
	print("Checking if user is already logged in..")
	
	-- Is User Logged In?
	local currentUser = false;
	currentLoginInfo = jsonHandler.getCurrentUserInformation()	
	if currentLoginInfo["currentUserID"] ~= nil then
		currentUser = true
		setCurrentUserID(		currentLoginInfo["currentUserID"]	)
		setCurrentUserPassword(	currentLoginInfo["currentPassword"]	)
		setCurrentUserEmail(	currentLoginInfo["currentEmail"]	)
		setCurrentUserName(		currentLoginInfo["currentUsername"]	)
	end
	return currentUser;
end
