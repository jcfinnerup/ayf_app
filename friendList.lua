module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Friend List")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------

-- My Name For Reference
local myName = portal.getCurrentUserName()

-- My Email For Reference
local myEmail = portal.getCurrentUserEmail()

-- Number Of Friends
local numberOfFriends = 0

-- Current View
local currentView = "peeps"
------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
background:setFillColor(230, 230, 230)
localGroup:insert(background)

-- Friend Banner
local friendBanner = display.newImage("images/friendBanner.png")
friendBanner.x = display.contentCenterX
localGroup:insert(friendBanner)

-- Back BTN
local backToAskBTN = display.newImage("images/backToAskOrange.png")
backToAskBTN:translate(15,friendBanner.y*.63)
localGroup:insert(backToAskBTN)

-- Add Friend BTN
local addFriendBTN = display.newImage("images/addFriend.png")
addFriendBTN:translate(display.contentWidth-115,friendBanner.y*.63)
localGroup:insert(addFriendBTN)

-- Ask Banner
local askBanner = display.newImage("images/askBanner.png")
askBanner.x, askBanner.y = display.contentCenterX, display.contentHeight-50
askBanner.isVisible = false
localGroup:insert(askBanner)

-- Add Circle BTN
local addCircleBTN = display.newImage("images/addCircle.png")
addCircleBTN.x, addCircleBTN.y = 570, 50
addCircleBTN.alpha = 0
localGroup:insert(addCircleBTN)

-- No Friends Text
local noFriendsText = display.newImage("images/noFriendsText.png")
noFriendsText.x, noFriendsText.y = display.contentCenterX, display.contentCenterY
noFriendsText.isVisible = false
localGroup:insert(noFriendsText)

-- Peeps BTN
local peepsBTN = display.newImage("images/peepsBTN.png")
peepsBTN.x, peepsBTN.y = peepsBTN.width/2, friendBanner.height+peepsBTN.height/2
peepsBTN.alpha = 0.8
peepsBTN.type = "left"
localGroup:insert(peepsBTN)

-- Requeste BTN
local requestsBTN = display.newImage("images/requestsBTN.png")
requestsBTN.x, requestsBTN.y = display.contentWidth-requestsBTN.width/2, friendBanner.height+requestsBTN.height/2
requestsBTN.type = "right"
localGroup:insert(requestsBTN)


----------------------------------------------
--		Table View Configuration w. func	--
----------------------------------------------

-- The TableView Variable For Reference
local questionTableView

-- Function For Reference
local downloadFriendList

-- Display Or Hide Ask Banner
local function displayOrHideAskBanner()	
	local numberPicked = table.maxn(receiverTable)
	if numberPicked == 0 then
		askBanner.isVisible = false
	elseif askBanner.isVisible == false then
		askBanner.isVisible = true
	end
end

-- Pick Friend
local function pickUnpickFriend(row)
	local picked = row.picked
	if picked.isVisible == true then
		picked.isVisible = false 
		receiverTable[row.index] = nil
	else
		receiverTable[row.index] = row
		picked.isVisible = true 
	end
	displayOrHideAskBanner()
end

-- Accept Friend Request
local function acceptFriendRequest(friendEmail)
	print("Accepting Friend Request")
	local hasBeenAccepted = portal.accpectFriendRequest(friendEmail)
	if hasBeenAccepted == "true" then
		downloadFriendList()
		print("Updating Friend List")
	else
		promt.promtUserWithMessage("Oooops", "You cannot answer your friend request now. Please check your internet connection")
	end
end

-- Table View Touch
local function tableViewTouch(e)
	if e.phase == "tap" or e.phase ==  "release" then
		local friendStatus = e.target.params["status"]
		local receiverEmail = string.lower(e.target.params["receiverEmail"])
		local requesterEmail = string.lower(e.target.params["requesterEmail"])
		if friendStatus == "pending" and myEmail == receiverEmail then
			acceptFriendRequest(requesterEmail)
			transition.to(e.target, {time=150, alpha=0.5})
			transition.to(e.target, {delay=150, time=150, alpha=1})
		end
	end
end

-- ScrollListener
local function scrollListener(e)
	-- Nothing
end

-- Row Rendering Options
-- this is what happenes with each individual row
local function renderTableViewTaps(e)
	
	-- Reference data
	local row = e.row
	local friendData = row.params
	local requesterUsername = 	friendData["requesterUsername"]
	local requesterEmail = 		string.lower(friendData["requesterEmail"])
	local receiverUsername = 	friendData["receiverUsername"]
	local receiverEmail = 		string.lower(friendData["receiverEmail"])
	local status = 				friendData["status"]
	local nameToDisplay = "Unknown"
	
	-- Picking Name to display and setting Email
	if myEmail == requesterEmail then
		nameToDisplay = receiverUsername
		row.email = receiverEmail
	else
		nameToDisplay = requesterUsername
		row.email = requesterEmail
	end
	
	-- Counting number of friends
	numberOfFriends = numberOfFriends + 1
	
	-- Friend Name Text
	local friendNameText = display.newText(row, nameToDisplay, display.contentWidth*0.06, 0, "PTSans-Regular", 35)
	friendNameText:setTextColor(75, 75, 75)
	friendNameText.y =  row.height*.4
	
	-- Email
	local friendEmail = display.newText(row, row.email, display.contentWidth*0.06, 0, "PTSans-Regular", 20)
	friendEmail:setTextColor(75, 75, 75)
	friendEmail.y =  row.height*.7
	
	-- Friend Color
	local friendColor = display.newRect(0, 0, 10, 75)
	friendColor:setFillColor(150, 200, 0)
	friendColor.x, friendColor.y = 20, row.height*.5
	row:insert(friendColor)
	
	-- Do Stuff
	local doStuffBTN = display.newImage("images/doStuff.png")
	doStuffBTN.x, doStuffBTN.y = display.contentWidth*.89, row.height*.5
	row:insert(doStuffBTN)
	
	-- Unfriend 
	--local unFriendBTN = display.newImage("images/unfriend.png")
	--unFriendBTN.x, unFriendBTN.y = display.contentWidth*.65, row.height*.5
	--row:insert(unFriendBTN)
	
	-- Is Friendrequest?
	if status == "pending" and receiverEmail == myEmail then

	-- Pending Color
		local pendingColor = display.newRect(0, 0, row.width, row.height)
		pendingColor:setFillColor(50, 230, 150)
		pendingColor.alpha = 0.5
		row:insert(pendingColor)
		
		-- Pending Text
		local pendingText = display.newText(row, "Pending.. Click to accept", display.contentWidth*.05, 0, "PTSans-Regular", 20)
		pendingText:setTextColor(50, 50, 50)
		pendingText.y = row.height*.75
		friendNameText.y = row.height*.4
		
		friendEmail.isVisible = false
		friendColor.isVisible = false
		doStuffBTN.isVisible = false
		
	-- Is Friend Pending?
	elseif status == "pending" and requesterEmail == myEmail then
		
		-- Pending Color
		local pendingColor = display.newRect(0, 0, row.width, row.height)
		pendingColor:setFillColor(50, 200, 255)
		pendingColor.alpha = 0.5
		row:insert(pendingColor)
		
		-- Pending Text
		local pendingText = display.newText(row, "Friend request sent", display.contentWidth*.05, 0, "PTSans-Regular", 20)
		pendingText:setTextColor(50, 50, 50)
		pendingText.y = row.height*.75
		friendNameText.y = row.height*.4
		
		friendEmail.isVisible = false
		friendColor.isVisible = false
		doStuffBTN.isVisible = false
		
	end
		
end


-- Table View for Friends
-- contains all the settings for the TableView
friendTableView = widget.newTableView
{
	top=friendBanner.y*2+peepsBTN.height,
	left=0,
	width=display.contentWidth,
	height = display.contentHeight-friendBanner.y*2,
	topPadding = 0,
	bottomPadding = display.contentHeight*.5,
	onRowRender = renderTableViewTaps,
	listener = scrollListener,
	onRowTouch = tableViewTouch,
	hideBackground = true,
}
localGroup:insert(friendTableView)
local currentPos = friendTableView:getContentPosition()
friendTableView:scrollToY({y=currentPos, time=0})

-- Fronting Things
askBanner:toFront()

------------------------------------------
--				Functions				--
------------------------------------------

-- Put Friends On Screen
local function putFriendsOnScreen(friends)
	for i = 1, #friends do
	    friendTableView:insertRow{
			params = friends[i],
			rowHeight = 120,
			rowColor={ 
				default={ 255, 255, 255 }, 
				over={ 255, 255, 255, 1 } 
			},
		}
	end
	-- Update row count
	totalRowCount = friendTableView:getNumRows()
	
	-- Set Rows Right
	friendTableView:scrollToY({y=currentPos, time=0})
end

-- Show No Friends
local function showNoFriends()
	noFriendsText.isVisible = true
end

-- Filter Friends
local function filterFriends(rawTable)
	local newTable = {}
	for i=1, #rawTable do
		if rawTable[i]["status"] == "accepted" and currentView == "peeps" then
			table.insert(newTable, rawTable[i])
		elseif rawTable[i]["status"] == "pending" and currentView == "requests" then
			table.insert(newTable, rawTable[i])
		end
	end
	return newTable
end

-- Download Friend List
downloadFriendList = function()
	
	-- Reset Number of friends
	numberOfFriends = 0
	
	-- Download questions and react to status
	local friendList = portal.getAllFriends()
	if friendList == "noconnect" then
		promt.promtUserWithMessage("Oooops", "We couldn't download your friend list. Please check your internet connection and try again")
	elseif friendList == "empty" then
		showNoFriends()
	else
		-- Hide No Friends Sign
		noFriendsText.isVisible = false
		
		-- Delete all Excisting Rows & Adding New
		friendTableView:deleteAllRows()
		local filteredTable = filterFriends(friendList)
		putFriendsOnScreen(filteredTable)
	end
end

-- Go Back
local function goBack(e)
	if e.phase == "began" then
		backToAskBTN.alpha=0.8
		display.getCurrentStage():setFocus(backToAskBTN)
	elseif e.phase == "ended" then
		backToAskBTN.alpha=1
		display.getCurrentStage():setFocus(nil)
		director:changeScene(lastScene, "fade")
	end
end
backToAskBTN:addEventListener("touch", goBack)

-- Go To Search For Friends
local function goToSearchForFriends(e)
	if e.phase == "began" then
		addFriendBTN.alpha = 0.8
		display.getCurrentStage():setFocus(addFriendBTN)
	elseif e.phase == "ended" then
		addFriendBTN.alpha = 1
		display.getCurrentStage():setFocus(nil)
		
		director:changeScene("addFriends")
	end
end
addFriendBTN:addEventListener("touch", goToSearchForFriends)

-- Post The Question via. portal
local function postViaPortal()
	local peopleToSendTo = getPeopleToSendTo()
	local toReturn = 0
	
	-- Tell Portal to send question
	if #peopleToSendTo > 0 then
		toReturn = portal.createAndSendQuestion(peopleToSendTo)
	end
	return toReturn
end


-- SwitchViews
local function switchViews(e)
	if e.phase == "began" then
		local type = e.target.type
		if peepsBTN.alpha == 1 and type == "left" then
			peepsBTN.alpha 		= 0.8
			requestsBTN.alpha	= 	1
			currentView			= "peeps"
			downloadFriendList()
		elseif peepsBTN.alpha == 0.8 and type == "right" then
			peepsBTN.alpha 		= 	1
			requestsBTN.alpha 	= 0.8
			currentView 		= "requests"
			downloadFriendList()
		end
	end
end
peepsBTN:addEventListener("touch", switchViews)
requestsBTN:addEventListener("touch", switchViews)



-- Download The Friend List when loaded
downloadFriendList()

-------------- BELOW DIRECTOR -------------
	return localGroup
end