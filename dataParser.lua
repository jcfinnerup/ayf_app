--[[
				This Class Is the Serializable Table Class that 
				will handle the question formatting and so on. 

]]

module(..., package.seeall)
print("Data Parser Ready...")


----------------------------------------------------------------------
--						PARSING DATA TO SEND						--
----------------------------------------------------------------------


-- Create Data for Creation of Question
function createQuestionData(senderID, username, questionText, answers, receivers)
	-- This is the data table
	local question = {
		-- Command
		["command"]			= "CREATE_QUESTION",
		
		-- Data
		["senderID"] 		= senderID,
		["username"]		= username,
		["questionText"] 	= questionText,
		["possibleAnswers"] = answers, 
		["receivers"] 		= receivers,
	}
	local stringToSend = json.encode( question )
	return stringToSend
end

-- Create Data For Creation of New User
function createNewUserData(username, password, email, phoneNumber, token)
	-- This is the data table
	local newUserData = {
		-- Command
		["command"]		= "CREATE_USER",
		
		-- Data
		["username"] 	= username,
		["phoneNumber"] = phoneNumber,
		["password"] 	= password,
		["email"]		= email,
		["deviceToken"] = token
	}
	local stringToSend = json.encode( newUserData )
	return stringToSend
end

-- Create Data to Receive Questions
function createGetAllQuestionsData(senderID, email)
	-- This is the data table
	local getQuestionsData = {
		["command"] 	= "PULL_QUESTIONS",
		
		-- Data
		["senderID"]= senderID,
		["email"]	= email
	}
	local stringToSend = json.encode(getQuestionsData)
	return stringToSend
end

-- Create Data to Verify User
function createVerifyUserData(emailOrUsername, password)
	-- This is the data table
	local verifyUserData = {
		-- Command
		["command"]		= "LOG_IN",
		
		-- Data
		["password"] 			= password,
		["emailOrUsername"]		= emailOrUsername
		
	}
	local stringToSend = json.encode(verifyUserData)
	return stringToSend
end

-- Create Data to Receive Complete Friend List
function createGetAllFriendsData(email, password)
	-- This is the data table
	local getAllFriendsData = {
		-- Command
		["command"]		= "PULL_FRIENDS",
		
		-- Data
		["email"] 		= email,
		["password"]	= password
	}
	local stringToSend = json.encode(getAllFriendsData)
	return stringToSend
end

-- Search For Friends
function searchForFriends(username, email, password, friendData)
	-- This is the data table
	local searchForFriendsData = {
		-- Command
		["command"]		= "SEARCH_FRIENDS",
		
		-- Data
		["username"]	= username,
		["email"] 		= email,
		["password"]	= password,
		["friendData"]	= friendData,
	}
	local stringToSend = json.encode(searchForFriendsData)
	return stringToSend
end

-- Add Friend
function createAddFriendData(username, email, password, friendUsername, friendEmail)
	-- This is the data table
	local addFriendData = {
		-- Command
		["command"]			= "ADD_FRIEND",
	
		-- Data
		["username"]		= username,
		["email"] 			= email,
		["password"]		= password,
		["friendUsername"]	= friendUsername,
		["friendEmail"]		= friendEmail
	}	
	local stringToSend = json.encode(addFriendData)
	return stringToSend
end

-- Accept Friend Request
function createAcceptFriendRequestData(username, email, password, friendEmail)
	-- This is the data table
	local acceptFriendData = {
		-- Command
		["command"]			= "ACCEPT_FRIEND_REQUEST",
	
		-- Data
		["username"]		= username,
		["email"] 			= email,
		["password"]		= password,
		["friendEmail"]		= friendEmail
	}	
	local stringToSend = json.encode(acceptFriendData)
	return stringToSend
end

-- Create Data To Submit Answer
function createDataToSubmitAnswer(username, email, password, questionID, answer)
	-- This is the data table
	local submitAnswerData = {
		-- Command
		["command"]			= "SUBMIT_ANSWER",
	
		-- Data
		["username"]		= username,
		["email"] 			= email,
		["password"]		= password,
		["questionID"]		= questionID,
		["answerIndex"]		= answer
	}	
	local stringToSend = json.encode(submitAnswerData)
	return stringToSend
end
----------------------------------------------------------------------
--						PARSING DATA RECEIVED						--
----------------------------------------------------------------------

-- Parsing Question Data Table. The Data is of JSON format
function parseQuestionData(questionData)
	local questionsTable = json.decode(questionData)
	return questionsTable
end

-- Parsing Friend List Data Table. The Data is of JSON format
function parseCompleteFriendList(friendData)
	local friendTable = json.decode(friendData)
	return friendTable
end

-- Parsing Search For Friends List
function parseSearchForFriendsList(searchResults)
	local peopleTable = json.decode(searchResults)
	return peopleTable
end

-- Parse Data On Login
function parseDataOnLogin(loginData)
	local loginTable = json.decode(loginData)
	return loginTable
end


