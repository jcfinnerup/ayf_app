--[[	

This screen is the LOGIN SCREEN
that means where the user presses SIGNIN or SIGNUP and so on.

]]

module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Login Screen")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------



------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newImageRect("images/loginScreen.png", 640, 1136)
background:translate(display.contentCenterX,background.height*.5)
background.alpha = 1
localGroup:insert(background)

-- Input Dialog
local inputDialog = display.newImage("images/loginBackground.png")
inputDialog:translate(0, 170)
localGroup:insert(inputDialog)

-- Credits Text 1
local creditsText1 = display.newText("Made in Denmark", 0, 0, "PTSans-Bold", 24)
creditsText1.x, creditsText1.y = display.contentCenterX, display.contentHeight*.9
creditsText1.alpha = 0.8
creditsText1:setTextColor(75, 75, 75)
localGroup:insert(creditsText1)

-- Credits Text 2
local creditsText2 = display.newText("Version: 0.607 København", 0, 0, "PTSans-Bold", 24)
creditsText2.x, creditsText2.y = display.contentCenterX, display.contentHeight*.95
creditsText2.alpha = 0.8
creditsText2:setTextColor(75, 75, 75)
localGroup:insert(creditsText2)

-- Email Field
local emailField = native.newTextField(20, inputDialog.y-inputDialog.height*.5+60, 600, 80)
emailField.hasBackground = false
emailField.inputType = "email"
emailField.font = native.newFont( "PTSans-Bold", 18 )
native.setKeyboardFocus( emailField )
localGroup:insert(emailField)

-- Password Field
local passwordField = native.newTextField(20, inputDialog.y-inputDialog.height*.5+210, 600, 80)
passwordField.isSecure = true
passwordField.hasBackground = false
passwordField.font = native.newFont( "PTSans-Bold", 18 )
localGroup:insert(passwordField)

------------------------------------------
--				Functions				--
------------------------------------------

-- Skip To Passwordfield
local function skipToPasswordField(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( passwordField )	
	end
end
emailField:addEventListener("userInput", skipToPasswordField)

-- Log User In
local function logUserIn(verification, password, email)
	
	-- Setting User Data
	portal.setCurrentUserID(verification["userID"])
	portal.setCurrentUserPassword(password)
	portal.setCurrentUserEmail(verification["email"])
	portal.setCurrentUserName(verification["username"])

	-- Set Up Automatic Login
	portal.setCurrentUserForAutomaticLogin()
		
	-- Change Scene
	director:changeScene("menu", "fade")
end

-- Check Password and Change Dialog
local function checkPasswordAndChangeDialog(e)
	if e.phase == "submitted" then
		local email = emailField.text
		local password = passwordField.text
		
		-- Verify User Through Server
		local verification = portal.verifyUser(email, password)
		
		-- Has User Been Verified?
		if verification == "false" then -- Wrong User
			promt.promtUserWithMessage("Oooops", "It looks like your Email or Password was typed incorrectly. Please try again")
		elseif verification == "noconnect" then -- No Internet
			promt.promtUserWithMessage("Oooops", "We couldn't verify you. Please check your internet connection and try again.")
		else -- Correct User
			logUserIn(verification, password, email)
		end
		
	end
end
passwordField:addEventListener("userInput", checkPasswordAndChangeDialog)


-------------- BELOW DIRECTOR -------------
	return localGroup
end