module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Question Input")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------

-- Set Last Scene
setLastScene("menu")

------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newImage("images/writeQuestionBackground.png")
localGroup:insert(background)

-- Banner 
local banner = display.newImage("images/writeQuestionBanner.png")
localGroup:insert(banner)
background:translate(0, banner.y+banner.height*.5)

-- Back To Menu Btn
local backToMenuBTN = display.newImage("images/backToAskOrange.png")
backToMenuBTN:translate(15,banner.y*.63)
localGroup:insert(backToMenuBTN)

-- Dialog
local dialog = display.newImage("images/writeQuestionDialog.png")
dialog.x = display.contentCenterX
dialog:translate(0, banner.y+banner.height*.5+20)
localGroup:insert(dialog)

-- Question Box
local questionBox = native.newTextField(0, 0, 555, 60)
questionBox.x, questionBox.y = dialog.x, dialog.y-80
questionBox.font = native.newFont( "PTSans-Regular", 18 )
questionBox.hasBackground = false
questionBox:setTextColor(50, 50, 50)
localGroup:insert(questionBox)
native.setKeyboardFocus(questionBox)


-- Answer One Field
local answerOneField = native.newTextField(0, 0, 550, 60)
answerOneField.x, answerOneField.y = dialog.x+10, dialog.y+40
answerOneField.font = native.newFont( "PTSans-Bold", 18 )
answerOneField:setTextColor(50, 50, 50)
answerOneField.hasBackground = false
localGroup:insert(answerOneField)


-- Answer Second Field
local answerSecondField = native.newTextField(0, 0, 550, 60)
answerSecondField.x, answerSecondField.y = dialog.x+10, dialog.y+120
answerSecondField.font = native.newFont( "PTSans-Bold", 18 )
answerSecondField:setTextColor(50, 50, 50)
answerSecondField.hasBackground = false
localGroup:insert(answerSecondField)


------------------------------------------
--				Functions				--
------------------------------------------



-- Register Data In Portal
local function registerInPortal()
	-- Setting a table for future functionallity of more answers
	local answersTable = {answerOneField.text, answerSecondField.text}
	
	portal.setCurrentQuestion(questionBox.text)
	portal.setCurrentAnswers(answersTable)
end

-- Go To Friend List
local function goToFriendList()
	registerInPortal()
	setLastScene("writeQuestion")
	director:changeScene("pickFriendsList", "fade")
end
-- Question Listener
local function questionListener(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( answerOneField )	
	end
end
questionBox:addEventListener("userInput", questionListener)

-- Answer 1 Listener
local function answerOneListener(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( answerSecondField )	
	end
end
answerOneField:addEventListener("userInput", answerOneListener)


-- Answer 2 Listener
local function answerTwoListener(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( nil )
		goToFriendList()
	end
end
answerSecondField:addEventListener("userInput", answerTwoListener)

-- Set Nil
local function setNil()
	native.setKeyboardFocus(nil)
end
background:addEventListener("touch", setNil)

-- Back to menu
local function goBack(e)
	if e.phase == "began" then
		backToMenuBTN.alpha=0.8
		display.getCurrentStage():setFocus(backToMenuBTN)
	elseif e.phase == "ended" then
		backToMenuBTN.alpha=1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("menu", "fade")
	end
end
backToMenuBTN:addEventListener("touch", goBack)

-------------- BELOW DIRECTOR -------------
	return localGroup
end