--[[
	
	FIRST CALLED Class 

]]

----------------------------------
--			Variables	 		--
----------------------------------






-- Last Scene
lastScene = "menu"

----------------------------------
--			Functions	 		--
----------------------------------

function setLastScene(scene)
	lastScene = scene
end

----------------------------------
--			Requirements 		--
----------------------------------
-- Director
director = require "director"

-- Promt User
promt = require "promtUser"

-- Json
json = require "json"

-- Portal
portal = require "portal"

-- Nofity
notify = require "notification"
notify.startListening()

------------------------------------
--			MAIN FUNCTION		  --
------------------------------------

-- MAIN GROUP
local mainGroup = display.newGroup();

-- MAIN FUNCTION
local function main()
	mainGroup:insert(director.directorView);
	-- WHERE TO GO FIRST
	currentUser = portal.getCurrentUserForAutomaticLogin()
	if currentUser == false then
		director:changeScene("groundZero");
	else
		director:changeScene("groundZero")
	end
	
	return true;
end
main();


