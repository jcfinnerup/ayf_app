--[[	

This screen is the Create User Screen
This is where the user signs up

]]

module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Create User Screen")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widgets
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------

-- New Username
local newEmail = ""

-- New Password
local newPassword = ""

-- New Username
local newUsername = ""

-- New Phone Number
local newPhoneNumber = "nophone"

-- Min password length
local minPass = 4

-- Min username Length
local minUsername = 3

-- Row Titles
local rowTitles = {"New Username", "Email", "New Password", "Repeat Password"}
local fieldTitles = {"username", "email", "password", "passwordRepeat"}
local inputFields = {}


------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newImageRect("images/loginScreen.png", 640, 1136)
background:translate(display.contentCenterX,background.height*.5)
background.alpha = 1
localGroup:insert(background)

-- Credits Text 1
local creditsText1 = display.newText("Made in Denmark", 0, 0, "PTSans-Bold", 24)
creditsText1.x, creditsText1.y = display.contentCenterX, display.contentHeight*.9
creditsText1.alpha = 0.8
creditsText1:setTextColor(75, 75, 75)
localGroup:insert(creditsText1)

-- Credits Text 2
local creditsText2 = display.newText("Version: 0.607 København", 0, 0, "PTSans-Bold", 24)
creditsText2.x, creditsText2.y = display.contentCenterX, display.contentHeight*.95
creditsText2.alpha = 0.8
creditsText2:setTextColor(75, 75, 75)
localGroup:insert(creditsText2)

-- Back Button
local backButton = display.newImage("images/downloadButton.png")
backButton:translate(15,50)
backButton.rotation = 90
localGroup:insert(backButton)

------------------------------------------
--			Table View Config			--
------------------------------------------

-- For Ref.
local newUserTableView

-- Render Table View Tabs
local function renderTableViewTaps(e)
	
	-- Specifing variables
	local row = e.row
	
	-- Background Tile
	local backgroundTile = display.newRect(0, 0, row.width, row.height-5)
	backgroundTile:setFillColor(86, 200, 150)
	row:insert(backgroundTile)
	
	-- Row title
	local rowTitle = display.newText(row, rowTitles[row.index], 4, 4, "PTSans-Regular", 28)
	rowTitle.x = row.width*.5
	rowTitle:setFillColor(255, 255, 255)
	
	-- Input Field
	local inputField = native.newTextField(10, 10, row.width-50, 80)
	inputField.hasBackground = false
	inputField.font = native.newFont( "PTSans-Regular", 16 )
	row:insert(inputField)
	inputFields[fieldTitles[row.index]] = inputField
	inputField.x = row.width*.5
	inputField.y = row.height*.6	
	
	-- Tiny Star 1
	local tinyStar1 = display.newImage("images/littleStar.png")
	row:insert(tinyStar1)
	tinyStar1.y = rowTitle.y
	tinyStar1.x = rowTitle.x - rowTitle.width*.5-tinyStar1.width
	
	-- Tiny Star 2
	local tinyStar2 = display.newImage("images/littleStar.png")
	row:insert(tinyStar2)
	tinyStar2.y = rowTitle.y
	tinyStar2.x = rowTitle.x + rowTitle.width*.5+tinyStar2.width
	
	if row.index > 2 then
		inputField.isSecure = true
	end
	
end

newUserTableView = widget.newTableView
{
	top=0,
	left=0,
	width=display.contentWidth,
	height = display.contentHeight,
	topPadding = 170,
	bottomPadding = display.contentHeight*.6,
	onRowRender = renderTableViewTaps,
	--listener = scrollListener,
	--onRowTouch = tableViewTouch,
	noLines = true,
	hideBackground = true,
}
localGroup:insert(newUserTableView)
newUserTableView:toFront()


------------------------------------------
--				Functions				--
------------------------------------------

-- Go Back
local function goBack(e)
	for i=1, 4 do
		inputFields[fieldTitles[i]].alpha = 0
	end
	director:changeScene("groundZero", "fade")
end
backButton:addEventListener("tap", goBack)

-- Generate Input Rows
local function generateInputRows()
	for i = 1, 4 do
	    newUserTableView:insertRow{
			--params = questions[i],
			rowHeight = 150,
			rowColor={ 
				default={ 255, 255, 255 }, 
				over={ 255, 255, 255, 1 }
			}
		}
	end
end
generateInputRows()

-- Location References
local usernameFieldY 		= newUserTableView:getRowAtIndex( 1 ).y-240
local emailFieldY 			= newUserTableView:getRowAtIndex( 2 ).y-240
local passwordFieldY 		= newUserTableView:getRowAtIndex( 3 ).y-240
local passwordRepeatFieldY 	= newUserTableView:getRowAtIndex( 4 ).y-240


local function checkStringFor(item, stringToCheck)
	local toReturn = string.find(stringToCheck, item)
	return toReturn;
end

------------------------------------------
--				Create User				--
------------------------------------------

-- Cache User and Log user in
local function cacheUser(response)
	print("User Succesfully Created")
	
	-- Set Current User Data
	portal.setCurrentUserID(response)
	portal.setCurrentUserPassword(password)
	portal.setCurrentUserEmail(newEmail)
	portal.setCurrentUserName(newUsername)
	
	-- Set Up Automatic Login
	portal.setCurrentUserForAutomaticLogin()
	
	-- Perform Transition
	promt.promtUserWithMessage("Welcome "..newUsername, "Time to Ask Your Friends")
		
	director:changeScene("menu")
end

-- Create or dismiss user
local function createOrDismissUser()
		print("Trying to Create User")
		

		-- Try To Create New User Through Portal
		local response = portal.createNewUser(newUsername, newPassword, newEmail, newPhoneNumber)		
		
		if response == "false" then -- User is taken
			promt.promtUserWithMessage("Boom!", "That email or username is already in use...")
			newUserTableView:scrollToY( { y=0-display.contentCenterY-usernameFieldY, time=600 } )
			native.setKeyboardFocus( inputFields["username"] )
		elseif response == "noconnect" then -- No Connection (noconnect)
			promt.promtUserWithMessage("Oooops", "We couldn't sign you up.. Please check your internet connection and try again")
			director:changeScene("groundZero")
		else -- User Can be Created
			cacheUser(response)
		end
end

------------------------------------------
--				Fields					--
------------------------------------------


---- Skip To Phone Number Field
local function skipToEmailField(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( inputFields["email"] )
		newUserTableView:scrollToY( { y=0-display.contentCenterY-emailFieldY, time=600 } )
	end
end
inputFields["username"]:addEventListener("userInput", skipToEmailField)

-- Skip To Passwordfield
local function skipToPasswordField(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( inputFields["password"] )	
		newUserTableView:scrollToY( { y=0-display.contentCenterY-passwordFieldY, time=600 } )
	end
end
inputFields["email"]:addEventListener("userInput", skipToPasswordField)

-- Skip To Passwordfield
local function skipToCheckPasswordField(e)
	if e.phase == "submitted" then
		native.setKeyboardFocus( inputFields["passwordRepeat"] )
	end
end
inputFields["password"]:addEventListener("userInput", skipToCheckPasswordField)


-- Check Stuff
local function checkStuff(e)
	if e.phase == "submitted" and string.len(inputFields["username"].text) < minUsername then
		promt.promtUserWithMessage("Boom!", "Please pick a username longer than "..(minUsername-1).." characters")
		newUserTableView:scrollToY( { y=0-display.contentCenterY-usernameFieldY, time=600 } )
		native.setKeyboardFocus( inputFields["username"] )
	elseif e.phase == "submitted" and checkStringFor("@", inputFields["email"].text) == nil then
		promt.promtUserWithMessage("Boom!", "Email needs @")
		newUserTableView:scrollToY( { y=0-display.contentCenterY-emailFieldY, time=600 } )
		native.setKeyboardFocus(inputFields["email"])
	elseif e.phase == "submitted" and string.len(inputFields["password"].text) < minPass then
		promt.promtUserWithMessage("Boom!", "Password must be more than "..(minPass-1).." characters")
		newUserTableView:scrollToY( { y=0-display.contentCenterY-passwordFieldY, time=600 } )
		native.setKeyboardFocus( inputFields["password"] )
	elseif e.phase == "submitted" and inputFields["password"].text ~= inputFields["passwordRepeat"].text then
		promt.promtUserWithMessage("Boom!", "Password must be identical")
		newUserTableView:scrollToY( { y=0-display.contentCenterY-passwordFieldY, time=600 } )
		native.setKeyboardFocus( inputFields["password"] )
	elseif e.phase == "submitted" then
		
		-- Save Stuff
		newUsername 	= inputFields["username"].text
		newEmail 		= inputFields["email"].text
		newPassword 	= inputFields["passwordRepeat"].text		
		
		createOrDismissUser()
	end
		
end
inputFields["passwordRepeat"]:addEventListener("userInput", checkStuff)





-------------- BELOW DIRECTOR -------------
	return localGroup
end