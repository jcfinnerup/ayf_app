module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Add Friends List")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"


------------------------------------------
--				Variables				--
------------------------------------------

-- Minimum number of letters for search
local minNumLettersForSearch = 3

-- Max Number of letters for search
local maxNumLettersForSearch = 30

-- My Username
local myUsername = portal.getCurrentUserName()


------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
background:setFillColor(230, 230, 230)
localGroup:insert(background)

-- Search Banner
local searchBanner = display.newImage("images/searchFriendsBanner.png")
searchBanner.x = display.contentCenterX
localGroup:insert(searchBanner)

-- Back Button
local backBTN = display.newImage("images/backButtonRene.png")
backBTN:translate(30, 60)
localGroup:insert(backBTN)

-- Search Field
local searchField = native.newTextField(30, searchBanner.y+30, 580, 60)
searchField.hasBackground = false
searchField.inputType = "email"
searchField.font = native.newFont( "PTSans-Regular", 18 )
native.setKeyboardFocus( searchField )
localGroup:insert(searchField)

----------------------------------------------
--		Table View Configuration w. func	--
----------------------------------------------
-- The TableView Variable For Reference
local questionTableView
local searchForPeople

-- Add Friend
local function addFriend(friendUsername, friendEmail)
	print("Adding Friend")
	local hasFriendBeenAdded = portal.addFriend(friendUsername, friendEmail)
	if hasFriendBeenAdded == "true" then
		searchForPeople(searchField.text)
	end
end

-- Table View Touch
local function tableViewTouch(e)
	if e.phase == "tap" or e.phase ==  "release" then
		local eventType = e.target.params["type"]
		if eventType == nil and myUsername ~= e.target.params["username"] and e.target.params["status"]~= "pending" then
			addFriend(e.target.params["username"], e.target.params["email"])
			transition.to(e.target, {time=150, alpha=0.5})
			transition.to(e.target, {delay=150, time=150, alpha=1})
		end
	end
end

-- ScrollListener
local function scrollListener(e)
	-- So far unused
end

-- Row Rendering Options
-- this is what happenes with each individual row
local function renderTableViewTaps(e)

	-- Reference data
	local row = e.row
	local peopleData = row.params
	local personsUsername = peopleData["username"]	
	local personEmail = peopleData["email"]
	local friendButtonUrl = "images/addFriendButton.png"
	local status = peopleData["status"]
	
	if personsUsername == myUsername then 
		friendButtonUrl = "images/youButton.png"
	elseif status == "pending" then
		friendButtonUrl = "images/pendingFriendButton.png"
	elseif status == "accepted" then
		friendButtonUrl = "images/friendButton.png"
	end
		
	-- Name of person text
	local nameOfPeronText = display.newText(row, personsUsername, display.contentWidth*0.05, 0, "PTSans-Bold", 35)
	nameOfPeronText:setTextColor(50, 50, 50)
	nameOfPeronText.y =  row.height*.3
		
	-- If People Do Exist
	if peopleData["type"] == nil then
		
		-- Email of Person Text
		local emailOfPersonText = display.newText(row, personEmail, display.contentWidth*0.05, 0, "PTSans-Regular", 30)
		emailOfPersonText:setTextColor(50, 50, 50)
		emailOfPersonText.y =  row.height*.7
		
		-- Add Friend Button
		local addFriendBTN = display.newImage(friendButtonUrl)
		addFriendBTN:translate(display.contentWidth*.74, row.height*.5-addFriendBTN.height*.5)
		row:insert(addFriendBTN)
	elseif peopleData["type"] == "noresults" then
		
		-- Email of Person Text
		local emailOfPersonText = display.newText(row, "Try other stuff", display.contentWidth*0.05, 0, "PTSans-Regular", 30)
		emailOfPersonText:setTextColor(50, 50, 50)
		emailOfPersonText.y =  row.height*.7
		
		-- No Results
		local noResultsBTN = display.newImage("images/noResultsButton.png")
		noResultsBTN:translate(display.contentWidth*.74, row.height*.5-noResultsBTN.height*.5)
		row:insert(noResultsBTN)
	end
end


-- Table View for Friends
-- contains all the settings for the TableView
peopleTable = widget.newTableView
{
	top=searchBanner.y*2,
	left=0,
	width=display.contentWidth,
	height = display.contentHeight-searchBanner.y*2,
	topPadding = 0,
	bottomPadding = display.contentHeight*.5,
	onRowRender = renderTableViewTaps,
	listener = scrollListener,
	onRowTouch = tableViewTouch,
	isBounceEnabled = true,
	hideBackground = true,
}
localGroup:insert(peopleTable)
local currentPos = peopleTable:getContentPosition()
peopleTable:scrollToY({y=currentPos, time=0})

------------------------------------------
--				Functions				--
------------------------------------------

-- Put People On Screen
local function putPeopleOnScreen(people)
	for i = 1, #people do
	    peopleTable:insertRow{
			params = people[i],
			rowHeight = 100,
			rowColor={ 
				default={ 255, 255, 255 }, 
				over={ 255, 255, 255, 1 } 
			},
		}
	end
	
	-- Update row count
	totalRowCount = peopleTable:getNumRows()
	
	-- Set Rows Right
	peopleTable:scrollToY({y=currentPos, time=0})
end


-- Delete Rows If Rows
local function deleteIfRows()
	if  peopleTable:getNumRows() ~= 0 then
		print("Deleting All Rows..")
		peopleTable:deleteAllRows()
	end
end

-- Insert Custom Data To Row
local function insertCustomData(customText, type)
	print("Displaying Custom Text")
	local searchForTable = {
		{
			["username"] = customText,
			["type"] = type
		}
	}
	deleteIfRows()
	putPeopleOnScreen(searchForTable)
end

-- Search For People - 
function searchForPeople(searchText)
	
	-- Download people from portal
	local peopleList = portal.searchForFriends(searchText)
	if peopleList == "noconnect" then
		-- noconnect
	elseif peopleList == "empty" then
		print("No People With These Credentials")
		insertCustomData("Sry! No results", "noresults")
	else
		-- Delete all Excisting Rows
		peopleTable:deleteAllRows()
		putPeopleOnScreen(peopleList)
		--native.setKeyboardFocus( nil )
	end
end

-- Remove Keyboard Focus
local function removeKeyboardFocus(e)
	if e.phase == "began" then
		native.setKeyboardFocus( nil )
	end
end
searchBanner:addEventListener("touch", removeKeyboardFocus)

-- Search For Friends
local function searchForFriends(e)
	if string.len(e.target.text) >= minNumLettersForSearch then
		if e.phase == "editing" then
			
			-- Checking for max number of letters
			if e.phase == "editing" then
				local txt = e.target.text            
			    if(string.len(txt)>maxNumLettersForSearch)then
			    	txt=string.sub(txt, 1, maxNumLettersForSearch)
			    	e.target.text=txt
			    end
			end
			insertCustomData("Search for: "..e.target.text.."?", "search")
		elseif e.phase == "submitted" then
			local searchText = e.target.text
			searchForPeople(searchText)
		end
	else
		deleteIfRows()
	end
end
searchField:addEventListener("userInput", searchForFriends)

-- Go Back
local function goBack(e)
	if e.phase == "began" then
		backBTN.alpha = 0.8
		display.getCurrentStage():setFocus(backBTN)
	elseif e.phase == "ended" then
		director:changeScene("friendList")
		backBTN.alpha = 1
		display.getCurrentStage():setFocus(nil)
	end
end
backBTN:addEventListener("touch", goBack)

-------------- BELOW DIRECTOR -------------
	return localGroup
end