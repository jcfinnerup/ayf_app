--[[	

This screen is the GROUND ZERO
that means where the user presses SIGNIN or SIGNUP and so on.

]]

module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Ground Zero")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------

------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newImageRect("images/loginScreen.png", 640, 1136)
background:translate(display.contentCenterX,background.height*.5)
background.alpha = 1
localGroup:insert(background)

-- Sign In Button
local signInBTN = display.newImage("images/signInButton.png")
signInBTN:translate(display.contentCenterX-signInBTN.width*.5, display.contentHeight*.3)
localGroup:insert(signInBTN)

-- Sign Up Button
local signUpBTN = display.newImage("images/signUpButton.png")
signUpBTN:translate(display.contentCenterX-signUpBTN.width*.5, display.contentHeight*.52)
localGroup:insert(signUpBTN)

-- Sign Up Text
local signUpText = display.newImage("images/loginText1.png")
signUpText:translate(display.contentCenterX-signUpText.width*.5, signUpBTN.y+signUpBTN.height*.5)
signUpText.alpha = 0.8
localGroup:insert(signUpText)

-- Sign In Text
local signInText = display.newImage("images/loginText2.png")
signInText:translate(display.contentCenterX-signInText.width*.5, signInBTN.y+signInBTN.height*.5)
signInText.alpha = 0.8
localGroup:insert(signInText)

-- Credits Text 1
local creditsText1 = display.newText("Made in Denmark", 0, 0, "PTSans-Bold", 24)
creditsText1.x, creditsText1.y = display.contentCenterX, display.contentHeight*.9
creditsText1.alpha = 0.8
creditsText1:setTextColor(75, 75, 75)
localGroup:insert(creditsText1)

-- Credits Text 2
local creditsText2 = display.newText("Version: 0.607 København", 0, 0, "PTSans-Bold", 24)
creditsText2.x, creditsText2.y = display.contentCenterX, display.contentHeight*.95
creditsText2.alpha = 0.8
creditsText2:setTextColor(75, 75, 75)
localGroup:insert(creditsText2)


------------------------------------------
--				Functioner				--
------------------------------------------


-- Go Sign In
local function goSignIn(e)
	if e.phase == "began" then
		signInBTN.alpha = 0.8
		display.getCurrentStage():setFocus(signInBTN)
	elseif e.phase == "moved" then
		if math.abs(e.x-signInBTN.x) > 250 or math.abs(e.y-signInBTN.y) > 150 then
			display.getCurrentStage():setFocus(nil)
			signInBTN.alpha = 1
		else
			display.getCurrentStage():setFocus(signInBTN)
			signInBTN.alpha = 0.8
		end
	elseif e.phase == "ended" then
		signInBTN.alpha = 1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("loginScreen", "fade")
	end
end
signInBTN:addEventListener("touch", goSignIn)

-- Go Sign In
local function goSignUp(e)
	if e.phase == "began" then
		signUpBTN.alpha = 0.8
		display.getCurrentStage():setFocus(signUpBTN)
	elseif e.phase == "moved" then
		if math.abs(e.x-signUpBTN.x) > 250 or math.abs(e.y-signUpBTN.y) > 150 then
			display.getCurrentStage():setFocus(nil)
			signUpBTN.alpha = 1
		else
			display.getCurrentStage():setFocus(signUpBTN)
			signUpBTN.alpha = 0.8
		end
	elseif e.phase == "ended" then
		signUpBTN.alpha = 1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("createUser", "fade")
	end
end
signUpBTN:addEventListener("touch", goSignUp)


-------------- BELOW DIRECTOR -------------
	return localGroup
end