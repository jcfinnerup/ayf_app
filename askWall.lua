--[[	
				This Class will handle the Questions you
				make yourself and the ones you receive

				the dataparser class contains informaiton
				about what 'datanames' that are fetched from
				the server when getting questions
]]

module(..., package.seeall)

function new()

	local localGroup = display.newGroup()

-------------- ABOVE DIRECTOR -------------
print("Loading Ask Wall")

------------------------------------------
--			   Requirements				--
------------------------------------------

-- Widget
local widget = require "widget"

------------------------------------------
--				Variables				--
------------------------------------------


-- Are Questions Loading - So No press on DOWNLOAD when already downloading
local areQuestionsLoading = false

-- Row Count - updated when new rows are added
local totalRowCount = 0

------------------------------------------
--			Display Objects				--
------------------------------------------

-- Background
local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
background:setFillColor(200, 255, 255)
localGroup:insert(background)

-- Ask Wall Banner
local askWallBanner = display.newImage("images/askWallTopBar.png")
askWallBanner:translate(0,0)
localGroup:insert(askWallBanner)

-- Download Button
local downloadBTN = display.newImage("images/downloadButton.png")
downloadBTN:translate(15,askWallBanner.y*.63)
localGroup:insert(downloadBTN)

-- Back To Menu Btn
local backToMenuBTN = display.newImage("images/backToAskButton.png")
backToMenuBTN:translate(display.contentWidth-115,askWallBanner.y*.63)
localGroup:insert(backToMenuBTN)

-- Credits Text 1
local creditsText1 = display.newText("Made in Denmark", 0, 0, "PT Sans", 24)
creditsText1.x, creditsText1.y = display.contentCenterX, display.contentHeight*.9
creditsText1:setTextColor(75, 75, 75)
localGroup:insert(creditsText1)

-- Credits Text 2
local creditsText2 = display.newText("Version: 0.607 København", 0, 0, "PT Sans", 24)
creditsText2.x, creditsText2.y = display.contentCenterX, display.contentHeight*.95
creditsText2:setTextColor(75, 75, 75)
localGroup:insert(creditsText2)

-- No Connection Bar
local noConnectionBar = display.newImage("images/noConnectionBar.png")
noConnectionBar:translate(0, askWallBanner.y+askWallBanner.height*.5-noConnectionBar.height)
localGroup:insert(noConnectionBar)

----------------------------------------------
--		Table View Configuration w. func	--
----------------------------------------------

-- The TableView Variable For Reference
local questionTableView

-- Table View Touch
-- Table View Touch
local function tableViewTouch(e)
	if e.phase == "tap" or e.phase ==  "release" then
		print("Showing results")
		local params = e.target.params
		local results = require "result"
		results.setQuestionParams(params)
		director:changeScene("result")
	end
end

-- ScrollListener
local function scrollListener(e)
	-- Nothing
end

-- Row Rendering Options
-- this is what happenes with each individual row
local function renderTableViewTaps(e)

	-- Resize question text
	local function resizeQuestionText(text)
		
		-- Sould Resize
		if string.len(text) > 35 then
			print("Resizing String")
			text = text:sub(1, 30)
			text = text..".."
		end
		return text
	end
	
	-- Should Append ?
	local function shouldAppend(text)
		if text:sub(string.len(text)) ~= "?" then
			text = text.."?"
		end
		return text
	end
	
	-- Format Receivers
	local function formatReceivers(receivers, numberOfReceivers)
		local receiversText = receivers[1]
		if numberOfReceivers == 2 then
			receiversText = receiversText.." and "..receivers[2]
		elseif numberOfReceivers == 3 then
			receiversText = receiversText..", "..receivers[2].." and "..receivers[3]
		elseif numberOfReceivers > 3 then
			receiversText = receiversText..", "..receivers[2].." and "..(numberOfReceivers-2).." others"
		end
		return receiversText
	end
	
	-- Reference data
    local row 					= e.row
	local questionData 			= row.params
	local receivers 			= questionData["receivers"]
	local actualAnswers 		= questionData["actualAnswers"]
	local numberOfActualAnswers = 0 -- To be specified in loop below
	local numberOfReceivers 	= #receivers
	local question 				= questionData["questionText"]
	local senderID 				= questionData["senderID"]
	local nameOfSender 			= questionData["usernameOfSender"]
	
	-- Practical Before Displaying
	questionData["completeQuestion"]	= shouldAppend(question)
	nameOfSender 						= portal.getNameOfSender(nameOfSender)
	local resizedQuestion 				= resizeQuestionText(question)
	resizedQuestion 					= shouldAppend(resizedQuestion)
	local formattedReceivers	 		= formatReceivers(receivers, numberOfReceivers)
	local senderColor 					= "Green" -- Default
	
	
	-- Accumulate Number Of First Answers
	for i=1, #actualAnswers[1] do
		if actualAnswers[1][i] ~= "0" then
			numberOfActualAnswers=numberOfActualAnswers+1
		end
	end
	
	-- Accumulate Number Of Second Answers
	for i=1, #actualAnswers[2] do
		if actualAnswers[2][i] ~= "0" then
			numberOfActualAnswers=numberOfActualAnswers+1
		end
	end
		
	
	row.alpha = 0
	transition.to(row, {time=500, alpha=1})
	-- Sender Text
	local senderText = display.newText(row, nameOfSender..":", display.contentWidth*.2, 0, "PTSans-Bold", 35)
	senderText:setTextColor(50, 50, 50)
	senderText.y =  row.height*.25
	
	-- Question Text
	local questionText = display.newText(row, resizedQuestion, senderText.x-senderText.width*.5, 0, "PTSans-Regular", 30)
	questionText:setTextColor(50, 50, 50)
	questionText.y = row.height*.53
	
	-- Receivers Text
	local receiversText = display.newText(row, "To: "..formattedReceivers, senderText.x-senderText.width*.5, 0, "PTSans-Regular", 24)
	receiversText:setTextColor(75, 75, 75)
	receiversText.y = row.height*.8
	
	-- Choosing color of Grid
	if nameOfSender == "You" then senderColor="Blue" end
	
	-- Status Grid
	local statusGrid = display.newImage("images/statusGrid"..senderColor..".png")
	statusGrid.y = row.height*.5
	statusGrid.x = display.contentWidth*.1
	row:insert(statusGrid)
	
	-- Receiver Number Text
	local receiverNumText = display.newText(row, numberOfReceivers+1, 0, 0, "PTSans-Bold", 20)
	receiverNumText:setTextColor(75, 75, 75)
	receiverNumText.x, receiverNumText.y = statusGrid.x, statusGrid.y+22
	
	-- Answer Number Text
	local answerNumberText = display.newText(row, numberOfActualAnswers, 0, 0, "PTSans-Bold", 20)
	answerNumberText:setTextColor(75, 75, 75)
	answerNumberText.x, answerNumberText.y = statusGrid.x, statusGrid.y-20
	
	-- To Result Button
	local toResultBTN = display.newImage("images/toResultsButton"..senderColor..".png")
	toResultBTN.y = row.height*.5
	toResultBTN.x = display.contentWidth*.92
	toResultBTN.isVisible = false
	row:insert(toResultBTN)
	
	-- Replies Text
	local repliesText = display.newText(row, "Answers", 0, 0, "PTSans-Regular", 16)
	repliesText:setTextColor(0, 0, 0)
	repliesText:translate(statusGrid.x-repliesText.width*.5, 5)
	repliesText.isVisible = false
	
end

-- Table View for Questions
-- contains all the settings for the TableView
questionTableView = widget.newTableView
{
	top=askWallBanner.y*2,
	left=0,
	width=display.contentWidth,
	height = display.contentHeight-askWallBanner.y*2,
	topPadding = 0,
	bottomPadding = display.contentHeight*.5,
	onRowRender = renderTableViewTaps,
	listener = scrollListener,
	onRowTouch = tableViewTouch,
	noLines = true,
	hideBackground = true,
}
localGroup:insert(questionTableView)
local currentPos = questionTableView:getContentPosition()
questionTableView:scrollToY({y=currentPos, time=0})

-- Update row count
totalRowCount = questionTableView:getNumRows()

-- Fronting The Banner
noConnectionBar:toFront()
askWallBanner:toFront()
downloadBTN:toFront()
backToMenuBTN:toFront()

------------------------------------------
--				Functions				--
------------------------------------------

-- Put Questions On Screen
local function putQuestionsOnScreen(questions)
	for i = 1, #questions do
	    questionTableView:insertRow{
			params = questions[i],
			rowHeight = 150,
			rowColor={ 
				default={ 255, 255, 255 }, 
				over={ 255, 255, 255, 1 } 
			},
		}
	end
	
	-- Update row count
	totalRowCount = questionTableView:getNumRows()
	
	-- Setting Row Right
	questionTableView:scrollToY({y=currentPos, time=0})
	
end

-- Connect Success Animation
local function showConnectSucces()
	transition.to(noConnectionBar, {time=500, y=askWallBanner.height-noConnectionBar.height*.5, transition=easing.outExpo})
	transition.to(downloadBTN, {time=1000,rotation=downloadBTN.rotation+360, transition=easing.inOutQuad,onComplete=function()
		downloadBTN.rotation=0 areQuestionsLoading=false 
	end})
end

-- Noconnect Animation
local function showNoConnect()
	transition.to(noConnectionBar, {time=500, y=askWallBanner.height+noConnectionBar.height*.5, transition=easing.outExpo})
	transition.to(downloadBTN, {time=100,rotation=50, transition=easing.outQuad})
	transition.to(downloadBTN, {delay=100, time=200,rotation=-50, transition=easing.outQuad})
	transition.to(downloadBTN, {delay=300,time=100,rotation=0, transition=easing.outQuad, onComplete=function() 
		areQuestionsLoading=false
	end})	
end

-- Download Questions - Handles button Animations
local function downloadQuestions()
	
	-- Download questions and react to status
	local questionsData = portal.getQuestions()
	if questionsData == "noconnect" then
		showNoConnect()
	elseif questionsData==nil then
		showConnectSucces()
	else
		-- Delete all Excisting Rows
		print("Deleting Old Questions..")
		questionTableView:deleteAllRows()
		putQuestionsOnScreen(questionsData)
		showConnectSucces()
		print("Getting Questions..")
	end
end

-- Download Button On Press
local function downloadBTNOnPress(e)
	if e.phase == "began" and areQuestionsLoading==false then
		display.getCurrentStage():setFocus(downloadBTN)
		downloadBTN.alpha=0.8
	elseif e.phase == "ended" and areQuestionsLoading==false then
		downloadBTN.alpha=1
		display.getCurrentStage():setFocus(nil)
		areQuestionsLoading = true
		downloadQuestions()
	end
end
downloadBTN:addEventListener("touch", downloadBTNOnPress)

-- Back To Ask Menu
local function backToAskMenu(e)
	if e.phase == "began" and areQuestionsLoading==false then
		display.getCurrentStage():setFocus(backToMenuBTN)
		backToMenuBTN.alpha=0.8
	elseif e.phase == "ended" and areQuestionsLoading==false then
		backToMenuBTN.alpha=1
		display.getCurrentStage():setFocus(nil)
		director:changeScene("menu", "fade")
	end
end
backToMenuBTN:addEventListener("touch", backToAskMenu)

-- Show Offline Questions
--* loads questions only from local JSON file
local function showOfflineQuestions()
	local offlineQuestionData = portal.getLocalQuestions()
	questionTableView:deleteAllRows()
	putQuestionsOnScreen(offlineQuestionData)
end
--showOfflineQuestions()

-- Download Questions When Wall Is Loaded
local downloader = coroutine.create(function()
	downloadQuestions()
end)
coroutine.resume(downloader)


-------------- BELOW DIRECTOR -------------
	return localGroup
end