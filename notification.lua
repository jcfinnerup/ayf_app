module(..., package.seeall)

--[[
This Module Handles All the push notification parts of the App. That means the token data, to incomming notifications. 
]]

print("Notifications Ready...")

------------------------------------------
--				Functions				--
------------------------------------------

-- Start To Listen to notifications
function startListening()
	local function notificationListener( event )
    	if ( event.type == "remote" ) then
        --handle the push notification
			system.vibrate()
    	elseif ( event.type == "remoteRegistration" ) then 
			portal.setDeviceToken(event.token)
    	end
	end
	Runtime:addEventListener( "notification", notificationListener )
end
